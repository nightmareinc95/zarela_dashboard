import React from "react"
import { Link } from "react-router-dom"
// import megamenuImg from "../../assets/images/megamenu-img.png"
import logo from "../../assets/images/logo.png"
import logoLightPng from "../../assets/images/logo.png"
import logoDark from "../../assets/images/logo-dark.png"
import { Row, Col } from "reactstrap"
import styled from "styled-components"

const Span = styled.span`
  font-size: 16px;
`

const BrandBox = ({ zarelaDay, zarelaDate }) => {
  return (
    <div className="navbar-brand-box w-100">
      <Link to="/" className="logo logo-dark">
        {/* <span className="logo-sm">
        <img src={logo} alt="" height="22" />
      </span> */}
        <span className="logo-lg">
          <img src={logoDark} alt="" height="30" />
        </span>
      </Link>

      <Row>
        <Col xs={6}>
          <Link to="/" className="logo logo-light">
            {/* <span className="logo-sm">
        <img src={logo} alt="" height="22" />
      </span> */}
            <span className="logo-lg">
              <img src={logoLightPng} alt="" height="70" />
            </span>
          </Link>
        </Col>
        <Col
          xs={6}
          className="d-flex flex-column justify-content-center align-items-start"
        >
          <Row>
          <Span>{zarelaDate}</Span>
          </Row>
          <Row>
            <Span>Zarela Day : {zarelaDay}th</Span>
          </Row>
        </Col>
      </Row>
    </div>
  )
}

export default BrandBox
