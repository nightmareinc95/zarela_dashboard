import { isNumber } from "lodash"
import React from "react"
import { Row, Col } from "reactstrap"
import styled from "styled-components"
import biobitIcon from "./../../../assets/icons/biobit-black.svg"

const Title = styled.span`
  font-size: ${props => (props.isMobile ? "10px" : "16px")};
  line-height: ${props => (props.isMobile ? "14px" : "28px")};
  font-weight: 700;
  color: #a3aed0;
  padding: ${props => (props.isMobile ? "0 2px" : "0")};
  max-width: ${props => (props.isMobile ? "90px" : "unset")}; ;
`

const Value = styled.span`
  color: ${props => (props.commingSoon ? "#F62D76" : "#1b2559")};
  font-size: ${props =>
    props.commingSoon ? "10px" : props.isMobile ? "10px" : "18px"};
  font-weight: 700;
  line-height: ${props => (props.isMobile ? "12px" : "28px")};
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
  padding: ${props => (props.isMobile ? "0 2px" : "0")};
`

const Column = styled(Col)`
  display: flex;
  flex-direction: column;
  justify-content: ${props => (props.isMobile ? "start" : "center")};
  border-left: ${props => (props.center ? "1px solid #E5E5E5" : "unset")};
  border-right: ${props => (props.center ? "1px solid #E5E5E5" : "unset")};
  height: ${props => (props.isMobile ? "75px" : "55.5px")};
  padding: ${props =>
    props.center ? "0 15px" : props.equivalent ? "0 0 0 15px" : "unset"};
  marign-right: ${props => (props.center ? "15px" : "0")};
  align-self: center;
`

const PercentText = styled.span`
  align-self: center;
  margin-right: 15px;
  color: ${props => props.color ?? "#000"};
`

const Icon = styled.i`
  color: ${props => props.color};
`
const Equivalent = styled.span`
  font-size: 12px;
  line-height: 30px;
  font-weight: 700;
  white-space: nowrap;
  color: #a3aed0;
`

const DataColumn = styled(Col)`
  flex-wrap: nowrap;
  width: fit-content;
  display: flex;
  flex-wrap: nowrap;
  align-items: center;
  padding: ${props => (props.isMobile ? "0 2px" : "0")};
`

const DataRow = styled(Row)`
  width: fit-content;
  justify-content: center;
  align-items: center;
  margin-top: ${props => (props.isMobile ? "6px" : "0")};
`

const TokenIcon = styled.img`
  width: ${[props => (props.isMobile ? "12px" : "22px")]};
  height: ${[props => (props.isMobile ? "12px" : "22px")]};
  margin: ${[props => (props.isMobile ? "0 3px 0 0" : "0 5px")]}; ;
`

const WidgetItem = ({
  title,
  bankBalance,
  subtitle,
  center,
  percent,
  equivalent,
  maxReward,
  commingSoon,
}) => {
  const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent)

  return (
    <Column
      xs={4}
      center={center}
      equivalent={maxReward ? true : false}
      isMobile={isMobile}
    >
      <Row>
        <Title isMobile={isMobile}>{title}</Title>
      </Row>
      <DataRow isMobile={isMobile}>
        <DataColumn isMobile={isMobile}>
          {commingSoon ? null : (
            <TokenIcon src={biobitIcon} isMobile={isMobile} />
          )}
          <Value commingSoon={commingSoon ?? false} isMobile={isMobile}>
            <>
              {bankBalance
                ? (bankBalance / Math.pow(10, 9)).toLocaleString()
                : maxReward
                ? maxReward
                : commingSoon
                ? commingSoon
                : null}
            </>
          </Value>
        </DataColumn>
        {percent && (
          <Col
            xs={4}
            className="d-flex justify-content-center align-items-center"
          >
            <Icon
              color={
                percent > 0
                  ? "#05CD99"
                  : percent < 0
                  ? "red"
                  : percent == 0 && "back"
              }
              className={`mdi ${
                percent > 0
                  ? "mdi-chevron-up"
                  : percent < 0
                  ? "mdi-chevron-down"
                  : percent == 0 && ""
              }`}
            ></Icon>

            <PercentText
              color={
                percent > 0
                  ? "#05CD99"
                  : percent < 0
                  ? "red"
                  : percent == 0 && "back"
              }
            >
              {percent > 0
                ? percent
                : percent < 0
                ? -1 * percent
                : percent == 0 && percent}
            </PercentText>
          </Col>
        )}

        <Col className="d-flex justify-content-start align-items-center p-0">
          {equivalent && <Equivalent>~ {equivalent}</Equivalent>}
        </Col>
      </DataRow>
    </Column>
  )
}

export default WidgetItem
