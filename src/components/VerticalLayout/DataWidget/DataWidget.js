import React from "react"
import styled from "styled-components"
import { Row, Col } from "reactstrap"
import WidgetItem from "./WidgetItem"

const Wrapper = styled.div`
  border: 1px solid #f1f1f1;
  height: 86px;
  width: ${props => (props.isMobile ? "100%" : "590px")};
  border-radius: 20px;
  padding: ${props => (props.isMobile ? "7px 19px" : "0 29px")};
  background-color: ${props => (props.isMobile ? "#fff" : "")};
  margin: ${props => (props.isMobile ? "0 0 25px 0" : "0 10px")};
`

const DataWidget = ({ bankBalance, BBIT, maxReward, isMobile }) => {
  return (
    <Wrapper
      {...{ isMobile }}
      className={isMobile ? "d-block d-md-none" : "d-none d-xl-block"}
    >
      <Row className="w-100 h-100 m-0">
        <WidgetItem title="Today Bank Balance" bankBalance={bankBalance} />
        <WidgetItem
          title="BBit Current Value"
          subtitle={"?"}
          // commingSoon={"to be announced soon"}
          // percent={2}
          // equivalent={5.6}
          maxReward={`${BBIT} DAI`}
          center
        />
        <WidgetItem title="Current Max. Reward" maxReward={maxReward} />
      </Row>
    </Wrapper>
  )
}

export default DataWidget
