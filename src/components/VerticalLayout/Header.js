import PropTypes from "prop-types"
import React, { useState, useEffect } from "react"
import styled from "styled-components"
import { convertToBBIT } from "helpers/convertToBBIT"
import logoLightPng from "../../assets/images/logo.png"

import { connect } from "react-redux"

//i18n
import { withTranslation } from "react-i18next"

import BrandBox from "./BrandBox"
import DataWidget from "./DataWidget/DataWidget"

// Redux Store
import {
  showRightSidebarAction,
  toggleLeftmenu,
  changeSidebarType,
} from "../../store/actions"
import { Col, Row } from "reactstrap"
import useBBIT_DAI from "helpers/hooks/useBBIT_DAI"

const Header = props => {
  const [zarelaDay, setZarelaDay] = useState()
  const [bankBalance, setBankBalance] = useState()
  const [maxReward, setMaxReward] = useState()
  const [zarelaDate, setZarelaDate] = useState()
  const [BBIT_DAI, isLoadingBBIT] = useBBIT_DAI()

  const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent)

  useEffect(() => {
    if (zarelaDay !== 0) {
      var date = new Date()
      let result =
        date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear()
      if (result.split("/")[0] !== "NaN") {
        setZarelaDate(result.split("/").join("."))
      }
    }
  }, [zarelaDay])

  useEffect(() => {
    if (props.contract) {
      /**
       * get Zarela Day
       */
      props.contract.methods
        .zarelaDayCounter()
        .call()
        .then(response => {
          setZarelaDay(response)
        })
        .catch(err => {
          console.error(err)
        })

      /**
       * get today bank balance
       */
      props.contract.methods
        .bankBalance()
        .call()
        .then(response => {
          setBankBalance(response)
        })
        .catch(err => {
          console.error(err)
        })

      /**
       * Max reward
       */
      props.contract.methods
        .maxUserDailyReward()
        .call()
        .then(response => {
          setMaxReward(convertToBBIT(response))
        })
        .catch(err => {
          console.error(err)
        })
    }
  }, [props.contract])

  const BrandBoxWrapper = styled.div`
    display: flex;
    width: 380px;
  `
  const MobileHeaderCol = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
  `

  const MobileTitleRow = styled.div`
    display: flex;
    padding: 15px;
    align-items: center;
  `

  const ZarelaIcon = styled.img`
    width: 120px;
  `

  const Span = styled.span`
    font-size: ${props => (props.bold ? "18px" : "14px")};
    font-weight: ${props => (props.bold ? "bold" : "normal")};
    color: ${props => (props.bold ? "#000000" : "")};
  `
  return (
    <React.Fragment>
      <header id="page-topbar">
        <div className="navbar-header m-0 p-0">
          {!isMobile ? (
            <>
              <BrandBoxWrapper>
                <BrandBox
                  zarelaDay={zarelaDay}
                  zarelaDate={zarelaDate ?? "--.--.--"}
                />
              </BrandBoxWrapper>
              <DataWidget
                bankBalance={bankBalance}
                BBIT={BBIT_DAI}
                maxReward={maxReward}
              />
            </>
          ) : (
            <MobileHeaderCol>
              <MobileTitleRow>
                <Col xs={6} className="p-0">
                  <ZarelaIcon src={logoLightPng} />
                </Col>
                <Col
                  xs={6}
                  className="d-flex flex-column justify-content-center align-items-end p-0"
                >
                  <Row>
                    <Span>{zarelaDate ?? "--.--.--"}</Span>
                  </Row>
                  <Row>
                    <Span>Zarela Day : {zarelaDay}th</Span>
                  </Row>
                </Col>
              </MobileTitleRow>
              <MobileTitleRow>
                <Span bold>Zarela Dashboard</Span>
              </MobileTitleRow>
            </MobileHeaderCol>
          )}
        </div>
      </header>
    </React.Fragment>
  )
}

Header.propTypes = {
  changeSidebarType: PropTypes.func,
  leftMenu: PropTypes.any,
  leftSideBarType: PropTypes.any,
  showRightSidebar: PropTypes.any,
  showRightSidebarAction: PropTypes.func,
  t: PropTypes.any,
  toggleLeftmenu: PropTypes.func,
}

// const mapStatetoProps = state => {
//   const { layoutType, showRightSidebar, leftMenu, leftSideBarType } =
//     state.Layout
//   return { layoutType, showRightSidebar, leftMenu, leftSideBarType }
// }

const mapStatetoProps = state => ({
  contract: state.Contract.contract,
})

export default connect(mapStatetoProps, {
  showRightSidebarAction,
  toggleLeftmenu,
  changeSidebarType,
})(withTranslation()(Header))
