import React from "react"
import Dashboard from "../pages/Dashboard/index"
import Pages404 from "../pages/Extra Pages/pages-404"

const userRoutes = [
  { path: "/", component: Dashboard },
  // this route should be at the end of all other routes
  { component: Pages404 },
]

export { userRoutes }
